﻿using System;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// 缓存接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataCache<T>
    {
        /// <summary>
        /// 增加数据
        /// </summary>
        /// <param name="value">数据</param>
        void Add(ReadOnlySpan<T> value);

        /// <summary>
        /// 从开头移除指定长度
        /// </summary>
        /// <param name="length"></param>
        void Remove(int length);

        /// <summary>
        /// 清空
        /// </summary>
        void Clear();

        /// <summary>
        /// 返回到目前为止写入基础缓冲区的数据
        /// </summary>
        ReadOnlySpan<T> WrittenSpan { get; }

        /// <summary>
        /// 返回到目前为止写入基础缓冲区的数据.
        /// </summary>
        ReadOnlyMemory<T> WrittenMemory { get; }

        /// <summary>
        /// 返回到目前为止写入基础缓冲区的数据量
        /// </summary>
        int WrittenCount { get; }
    }
}
