﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: ByteHelper.cs
* 文件说明: 包括字节顺序的数据与字节数组转换帮助类
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2022-11-01: V1.0.0 增加文件说明
******************************************************************************/

using System;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// Bit位操作帮助类
    /// </summary>
    public static class BitHelper
    {
        /// <summary>
        /// 获取二进制字符串
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>字符串</returns>
        public static string BitsString(byte value)
        {
            int size = sizeof(byte) * 8;
            return Convert.ToString(value, 2).ToUpper().PadLeft(size, '0');
        }

        /// <summary>
        /// 获取二进制字符串
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>字符串</returns>
        public static string BitsString(ushort value)
        {
            int size = sizeof(ushort) * 8;
            return Convert.ToString(value, 2).ToUpper().PadLeft(size, '0');
        }

        /// <summary>
        /// 获取二进制字符串
        /// </summary>
        /// <param name="value">值</param>
        /// <returns>字符串</returns>
        public static string BitsString(uint value)
        {
            int size = sizeof(uint) * 8;
            return Convert.ToString(value, 2).ToUpper().PadLeft(size, '0');
        }

        /// <summary>
        /// 获取单字节值
        /// </summary>
        /// <param name="value">二进制字符串</param>
        /// <returns>单字节值</returns>
        public static byte BitsByte(string value)
        {
            int size = sizeof(byte) * 8;
            if (value.Length > size) value = value.Substring(value.Length - size, size);
            return Convert.ToByte(value, 2);
        }

        /// <summary>
        /// 获取无符号双字节值
        /// </summary>
        /// <param name="value">二进制字符串</param>
        /// <returns>无符号双字节值</returns>
        public static ushort BitsUShort(string value)
        {
            int size = sizeof(ushort) * 8;
            if (value.Length > size) value = value.Substring(value.Length - size, size);
            return Convert.ToUInt16(value, 2);
        }

        /// <summary>
        /// 获取无符号四字节值
        /// </summary>
        /// <param name="value">二进制字符串</param>
        /// <returns>无符号四字节值</returns>
        public static uint BitsUInt(string value)
        {
            int size = sizeof(uint) * 8;
            if (value.Length > size) value = value.Substring(value.Length - size, size);
            return Convert.ToUInt32(value, 2);
        }

        /// <summary>
        /// 数值按位操作
        /// </summary>
        /// <param name="state">数值</param>
        /// <param name="flag">位</param>
        /// <param name="value">是否有效</param>
        /// <returns></returns>
        public static int SetFlag(int state, int flag, bool value)
        {
            if (value)
            {
                state |= flag;
            }
            else
            {
                state &= ~flag;
            }

            return state;
        }

        /// <summary>
        /// 数值的某位是否有效
        /// </summary>
        /// <param name="state">数值</param>
        /// <param name="flag">位</param>
        /// <returns>是否有效</returns>
        public static bool HasFlag(int state, int flag)
        {
            return (state & flag) != 0;
        }
    }
}
