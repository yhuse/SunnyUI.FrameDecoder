﻿using System.ComponentModel;

namespace Sunny.FrameDecoder;

/// <summary>
/// 数据类型
/// </summary>
public enum FrameDataType
{
    /// <summary>
    /// 字节数组
    /// </summary>
    [Description("十六进制字节数组")]
    HEX = 0,

    /// <summary>
    /// 字符串
    /// </summary>
    [Description("ASCII编码字符串")]
    ASCII = 1
}