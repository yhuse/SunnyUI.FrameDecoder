﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: ValueLengthType.cs
* 文件说明: 数据帧解码器定义类
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2022-11-01: V1.0.0 增加文件说明
******************************************************************************/

using System.ComponentModel;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// 数据长度类型
    /// </summary>
    public enum LengthType
    {
        /// <summary>
        /// 字节，单字节
        /// </summary>
        [Description("字节，单字节")]
        Byte = 1,

        /// <summary>
        /// 短整数，双字节
        /// </summary>
        [Description("短整数，双字节")]
        UShort = 2,

        /// <summary>
        /// 整数，四字节
        /// </summary>
        [Description("整数，四字节")]
        UInt = 4
    }
}
