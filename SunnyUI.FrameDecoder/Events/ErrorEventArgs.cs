﻿using System;

namespace Sunny.FrameDecoder;

/// <summary>
/// 字节解码错误消息
/// </summary>
public struct ByteDecoderExceptionEventArgs
{
    /// <summary>
    /// 消息
    /// </summary>
    public string Message { get; }

    /// <summary>
    /// 数据
    /// </summary>
    public byte[] Value { get; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="value"></param>
    public ByteDecoderExceptionEventArgs(string message, byte[] value)
    {
        Message = message;
        Value = value;
    }
}

/// <summary>
/// 字符串解码错误消息
/// </summary>
public class StringDecoderExceptionEventArgs : EventArgs
{
    /// <summary>
    /// 消息
    /// </summary>
    public string Message { get; }

    /// <summary>
    /// 数据
    /// </summary>
    public string Value { get; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="message"></param>
    /// <param name="value"></param>
    public StringDecoderExceptionEventArgs(string message, string value)
    {
        Message = message;
        Value = value;
    }
}

/// <summary>
/// 解码错误事件类
/// </summary>
/// <param name="sender">对象</param>
/// <param name="e">解码错误事件类</param>
public delegate void OnByteFrameDecoderError(object sender, ByteDecoderExceptionEventArgs e);