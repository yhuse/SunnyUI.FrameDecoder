﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: DecoderDefine.cs
* 文件说明: 数据帧解码器定义类
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2022-11-01: V1.0.0 增加文件说明
******************************************************************************/

using System;

namespace Sunny.FrameDecoder;

/// <summary>
/// 字符串数据事件类
/// </summary>
public class StringEventArgs : EventArgs
{
    /// <summary>
    /// 数据
    /// </summary>
    public string Value { get; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="value">数据</param>
    public StringEventArgs(string value)
    {
        Value = value;
    }
}

/// <summary>
/// 字符串解码数据事件类
/// </summary>
public class StringFrameEventArgs : StringEventArgs
{
    /// <summary>
    /// 解码器
    /// </summary>
    public BaseStringFrameDecoder Decoder { get; }

    /// <summary>
    /// 整帧数据，包含分隔符、标签、数据头、数据尾部等
    /// </summary>
    public string Body { get; }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="decoder">解码器</param>
    /// <param name="value">数据</param>
    public StringFrameEventArgs(BaseStringFrameDecoder decoder, string value) : base(value)
    {
        Decoder = decoder;
        Body = value;
    }

    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="decoder">解码器</param>
    /// <param name="value">数据</param>
    /// <param name="body">整帧数据</param>
    public StringFrameEventArgs(BaseStringFrameDecoder decoder, string value, string body) : base(value)
    {
        Decoder = decoder;
        Body = body;
    }
}