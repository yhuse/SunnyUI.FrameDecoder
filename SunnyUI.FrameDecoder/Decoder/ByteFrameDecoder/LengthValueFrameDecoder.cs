﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: LengthValueFrameDecoder.cs
* 文件说明: 数据长度、数据 - 数据帧解码器
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2020-01-01: V1.0.0 增加文件说明
******************************************************************************/

using System;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// 数据长度、数据 - 数据帧解码器
    /// </summary>
    public class LengthValueFrameDecoder : BaseByteFrameDecoder
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="lengthType">数据长度类型</param>
        /// <param name="valueLengthIsLittleEndian">数据长度字节顺序</param>
        /// <param name="isFullLength">数据长度是否包含数据长度的长度，false为仅数据长度</param>
        /// <param name="maxFrameLength">最大数据长度，仅判断数据长度</param>
        public LengthValueFrameDecoder(LengthType lengthType, bool valueLengthIsLittleEndian = true, bool isFullLength = false, int maxFrameLength = 0)
        {
            LengthType = lengthType;
            IsFullLength = isFullLength;
            MaxFrameLength = maxFrameLength;
            _valueLengthIsLittleEndian = valueLengthIsLittleEndian;

            FrameExceptDataLength = lengthType.Size();
        }

        ///<inheritdoc/>
        public override BaseByteFrameDecoder Clone()
        {
            return new LengthValueFrameDecoder(LengthType, _valueLengthIsLittleEndian, IsFullLength, MaxFrameLength);
        }

        private readonly bool _valueLengthIsLittleEndian;

        /// <summary>
        /// 数据长度类型
        /// </summary>
        public LengthType LengthType { get; }

        /// <summary>
        /// 数据长度是否包含数据长度的长度，false为仅数据长度
        /// </summary>
        public bool IsFullLength { get; }

        /// <summary>
        /// 准备数据解码
        /// </summary>
        /// <param name="data">输入数据</param>
        /// <returns>判断此数据是否可以解码</returns>
        protected override bool PrepareDecode(ReadOnlySpan<byte> data)
        {
            if (data.Length == 0)
            {
                DecoderError("The data to be decoded is null.", data);
                return false;
            }

            if (Cache.WrittenCount > 0)
            {
                return true;
            }

            if (data.Length < FrameExceptDataLength)
            {
                return false;
            }

            int len = data.Slice(0, LengthType.Size()).GetLengthValue(_valueLengthIsLittleEndian);
            int dataLen = IsFullLength ? len - LengthType.Size() : len;

            if (dataLen < 0)
            {
                return false;
            }

            if (MinFrameLength > 0 && dataLen < MinFrameLength)
            {
                DecoderError("The data length is less than the minimum length", data);
                return false;
            }

            if (MaxFrameLength > 0 && MaxFrameLength > MinFrameLength && dataLen > MaxFrameLength)
            {
                DecoderError("The data length is greater than the maximum length.", data);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 解码函数
        /// </summary>
        protected override void Decoding()
        {
            if (Cache.WrittenCount < FrameExceptDataLength) return;

            int len = Cache.WrittenSpan.GetLengthValue(0, LengthType.Size(), _valueLengthIsLittleEndian);
            int dataLen = IsFullLength ? len - FrameExceptDataLength : len;

            if (dataLen < 0)
            {
                Reset();
                return;
            }

            while (dataLen >= 0)
            {
                if (MinFrameLength > 0 && dataLen < MinFrameLength)
                {
                    DecoderError("The data length is less than the minimum length", Cache.WrittenSpan);
                    Reset();
                    break;
                }

                if (MaxFrameLength > 0 && MaxFrameLength > MinFrameLength && dataLen > MaxFrameLength)
                {
                    DecoderError("The data length is greater than the maximum length.", Cache.WrittenSpan);
                    Reset();
                    break;
                }

                int allLen = dataLen + FrameExceptDataLength;
                if (allLen <= Cache.WrittenCount)
                {
                    LastDecodedTime = DateTime.Now;
                    ByteFrameEventArgs e = new(this, ByteFrameParams.LengthParams(len),
                       LengthType.Size(), dataLen, GetResult(0, allLen));
                    InvokeOnDecoder(e);
                }
                else
                {
                    break;
                }

                Cache.Remove(allLen);
                if (Cache.WrittenCount < FrameExceptDataLength) return;

                len = Cache.WrittenSpan.GetLengthValue(0, LengthType.Size(), _valueLengthIsLittleEndian);
                dataLen = IsFullLength ? len - FrameExceptDataLength : len;
            }
        }

        /// <summary>
        /// 根据数据创建完整帧数据
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns>完整帧数据</returns>
        public byte[] CreateFrame(ReadOnlySpan<byte> data)
        {
            byte[] frame = new byte[FrameExceptDataLength + data.Length];
            switch (LengthType)
            {
                case LengthType.Byte:
                    DecoderHelper.TryWriteByte(frame, 0, (byte)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length));
                    break;
                case LengthType.UShort:
                    DecoderHelper.TryWriteUShort(frame, 0, (ushort)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length), _valueLengthIsLittleEndian);
                    break;
                case LengthType.UInt:
                    DecoderHelper.TryWriteUInt(frame, 0, (uint)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length), _valueLengthIsLittleEndian);
                    break;
            }

            data.CopyTo(frame.AsSpan().Slice(FrameExceptDataLength));
            return frame;
        }

        /// <summary>
        /// 解析完整帧数据
        /// </summary>
        /// <param name="frame">完整帧数据</param>
        /// <returns>解析是否成功</returns>
        public override ByteFrameEventArgs DecodeFrame(ReadOnlySpan<byte> frame)
        {
            if (frame.Length >= LengthType.Size())
            {
                int len = frame.Slice(0, LengthType.Size()).GetLengthValue(_valueLengthIsLittleEndian);
                int frameLen = IsFullLength ? len : len + LengthType.Size();
                if (frameLen == frame.Length)
                {
                    return new(this, ByteFrameParams.LengthParams(len),
                        LengthType.Size(), frame.Length - FrameExceptDataLength, frame.ToArray());
                }
            }

            return new ByteFrameEventArgs(null, ByteFrameParams.Empty, frame.ToArray());
        }
    }
}
