﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: TagLengthValueFrameDecoder.cs
* 文件说明: 标签、数据长度、数据 - 数据帧解码器
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2020-01-01: V1.0.0 增加文件说明
******************************************************************************/

using System;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// 标签、数据长度、数据 - 数据帧解码器
    /// </summary>
    public class TagLengthValueFrameDecoder : BaseByteFrameDecoder
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tagLength">标签长度</param>
        /// <param name="lengthType">数据长度类型</param>
        /// <param name="valueLengthIsLittleEndian">数据长度字节顺序</param>
        /// <param name="isFullLength">数据长度是否包含标签、数据长度的长度，false为仅数据长度</param>
        /// <param name="maxFrameLength">最大数据长度，仅判断数据长度</param>
        public TagLengthValueFrameDecoder(int tagLength, LengthType lengthType, bool valueLengthIsLittleEndian = true, bool isFullLength = false, int maxFrameLength = 0)
        {
            if (tagLength < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(tagLength), $"Tag length must be greater than or equal to zero.");
            }

            TagLength = tagLength;
            LengthType = lengthType;
            IsFullLength = isFullLength;
            MaxFrameLength = maxFrameLength;
            _valueLengthIsLittleEndian = valueLengthIsLittleEndian;
            FrameExceptDataLength = TagLength + LengthType.Size();
        }

        ///<inheritdoc/>
        public override BaseByteFrameDecoder Clone()
        {
            return new TagLengthValueFrameDecoder(TagLength, LengthType, _valueLengthIsLittleEndian, IsFullLength, MaxFrameLength);
        }

        private readonly bool _valueLengthIsLittleEndian;

        /// <summary>
        /// 标签长度
        /// </summary>
        public int TagLength { get; }

        /// <summary>
        /// 数据长度类型
        /// </summary>
        public LengthType LengthType { get; }

        /// <summary>
        /// 数据长度是否包含标签、数据长度的长度，false为仅数据长度
        /// </summary>
        public bool IsFullLength { get; }

        /// <summary>
        /// 解码函数
        /// </summary>
        protected override void Decoding()
        {
            if (Cache.WrittenCount < FrameExceptDataLength) return;

            byte[] tag = Cache.WrittenSpan.Slice(0, TagLength).ToArray();
            int len = Cache.WrittenSpan.GetLengthValue(TagLength, LengthType.Size(), _valueLengthIsLittleEndian);
            int dataLen = IsFullLength ? len - FrameExceptDataLength : len;

            if (dataLen < 0)
            {
                Reset();
                return;
            }

            while (dataLen >= 0)
            {
                if (MinFrameLength > 0 && dataLen < MinFrameLength)
                {
                    DecoderError("The data length is less than the minimum length", Cache.WrittenSpan);
                    Reset();
                    break;
                }

                if (MaxFrameLength > 0 && MaxFrameLength > MinFrameLength && dataLen > MaxFrameLength)
                {
                    DecoderError("The data length is greater than the maximum length.", Cache.WrittenSpan);
                    Reset();
                    break;
                }

                int allLen = dataLen + TagLength + LengthType.Size();
                if (allLen <= Cache.WrittenCount)
                {
                    LastDecodedTime = DateTime.Now;
                    ByteFrameEventArgs e = new(this, ByteFrameParams.TagLengthParams(tag, len),
                        TagLength + LengthType.Size(), dataLen, GetResult(0, allLen));
                    InvokeOnDecoder(e);
                }
                else
                {
                    break;
                }

                Cache.Remove(allLen);
                if (Cache.WrittenCount < FrameExceptDataLength) return;

                tag = Cache.WrittenSpan.Slice(0, TagLength).ToArray();
                len = Cache.WrittenSpan.GetLengthValue(TagLength, LengthType.Size(), _valueLengthIsLittleEndian);
                dataLen = IsFullLength ? len - FrameExceptDataLength : len;
            }
        }

        /// <summary>
        /// 根据数据创建完整帧数据
        /// </summary>
        /// <param name="tags">标签</param>
        /// <param name="data">数据</param>
        /// <returns>完整帧数据</returns>
        public byte[] CreateFrame(byte[] tags, ReadOnlySpan<byte> data)
        {
            byte[] frame = new byte[FrameExceptDataLength + data.Length];
            tags.AsSpan().CopyTo(frame.AsSpan());
            switch (LengthType)
            {
                case LengthType.Byte:
                    DecoderHelper.TryWriteByte(frame, tags.Length, (byte)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length));
                    break;
                case LengthType.UShort:
                    DecoderHelper.TryWriteUShort(frame, tags.Length, (ushort)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length), _valueLengthIsLittleEndian);
                    break;
                case LengthType.UInt:
                    DecoderHelper.TryWriteUInt(frame, tags.Length, (uint)(IsFullLength ? data.Length + FrameExceptDataLength : data.Length), _valueLengthIsLittleEndian);
                    break;
            }

            data.CopyTo(frame.AsSpan().Slice(tags.Length + LengthType.Size()));
            return frame;
        }

        /// <summary>
        /// 解析完整帧数据
        /// </summary>
        /// <param name="frame">完整帧数据</param>
        /// <returns>解析是否成功</returns>
        public override ByteFrameEventArgs DecodeFrame(ReadOnlySpan<byte> frame)
        {
            if (frame.Length >= FrameExceptDataLength)
            {
                byte[] tag = frame.Slice(0, TagLength).ToArray();
                int len = frame.Slice(TagLength, LengthType.Size()).GetLengthValue(_valueLengthIsLittleEndian);
                int frameLen = IsFullLength ? len : len + FrameExceptDataLength;
                if (frameLen == frame.Length)
                {
                    return new(this, ByteFrameParams.TagLengthParams(tag, len),
                        TagLength + LengthType.Size(), frame.Length - FrameExceptDataLength, frame.ToArray());
                }
            }

            return new ByteFrameEventArgs(null, ByteFrameParams.Empty, frame.ToArray());
        }
    }
}
