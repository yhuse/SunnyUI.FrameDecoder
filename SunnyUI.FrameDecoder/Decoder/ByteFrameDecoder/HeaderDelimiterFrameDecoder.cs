﻿/******************************************************************************
* SunnyUI.FrameDecoder 开源TCP、串口数据解码库。
* CopyRight (C) 2022-2023 ShenYongHua(沈永华).
* QQ群：56829229 QQ：17612584 EMail：SunnyUI@qq.com
*
* Blog:   https://www.cnblogs.com/yhuse
* Gitee:  https://gitee.com/yhuse/SunnyUI.FrameDecoder
*
* SunnyUI.FrameDecoder.dll can be used for free under the MIT license.
* If you use this code, please keep this note.
* 如果您使用此代码，请保留此说明。
******************************************************************************
* 文件名称: HeaderDelimiterFrameDecoder.cs
* 文件说明: 头部分隔符 - 数据帧解码器
* 当前版本: V1.0
* 创建日期: 2022-11-01
*
* 2022-11-01: V1.0.0 增加文件说明
******************************************************************************/

using System;

namespace Sunny.FrameDecoder
{
    /// <summary>
    /// 头部分隔符 - 数据帧解码器
    /// </summary>
    public class HeaderDelimiterFrameDecoder : BaseByteFrameDecoder
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="headers">数据头部</param>
        /// <param name="maxFrameLength">最大数据长度，仅判断数据长度</param>
        public HeaderDelimiterFrameDecoder(byte[] headers, int maxFrameLength = 0)
        {
            if (headers == null || headers.Length == 0)
            {
                throw new ArgumentNullException(nameof(headers), $"Headers cannot be empty.");
            }

            _headerLength = headers.Length;
            Headers = headers;
            MaxFrameLength = maxFrameLength;
            FrameExceptDataLength = _headerLength;
        }

        ///<inheritdoc/>
        public override BaseByteFrameDecoder Clone()
        {
            return new HeaderDelimiterFrameDecoder(Headers, MaxFrameLength);
        }

        private readonly int _headerLength;

        /// <summary>
        /// 分隔符。
        /// </summary>
        public byte[] Headers { get; }

        /// <summary>
        /// 解码函数
        /// </summary>
        protected override void Decoding()
        {
            if (Cache.WrittenCount < FrameExceptDataLength) return;
            int index = Cache.WrittenSpan.IndexOf(Headers);
            if (index > 0) Cache.Remove(index);
            if (Cache.WrittenCount < FrameExceptDataLength) return;
            index = Cache.WrittenSpan.Slice(_headerLength).IndexOf(Headers);

            while (index >= 0)
            {
                if (MinFrameLength > 0 && index < MinFrameLength)
                {
                    DecoderError("The data length is less than the minimum length", Cache.WrittenSpan);
                    Cache.Remove(index);
                    continue;
                }

                if (MaxFrameLength > 0 && MaxFrameLength > MinFrameLength && index > MaxFrameLength)
                {
                    DecoderError("The data length is greater than the maximum length.", Cache.WrittenSpan);
                    Cache.Remove(index);
                    continue;
                }

                LastDecodedTime = DateTime.Now;
                ByteFrameEventArgs e = new(this, ByteFrameParams.Empty, _headerLength, index, GetResult(0, index + FrameExceptDataLength));
                InvokeOnDecoder(e);

                Cache.Remove(_headerLength + index);
                if (Cache.WrittenCount < FrameExceptDataLength) break;
                index = Cache.WrittenSpan.Slice(_headerLength).IndexOf(Headers);
            }
        }

        /// <summary>
        /// 准备数据解码
        /// </summary>
        /// <param name="data">输入数据</param>
        /// <returns>判断此数据是否可以解码</returns>
        protected override bool PrepareDecode(ReadOnlySpan<byte> data)
        {
            if (data.Length == 0)
            {
                DecoderError("The data to be decoded is null.", data);
                return false;
            }

            if (Cache.WrittenCount > 0)
            {
                return true;
            }

            if (data.IndexOf(Headers.AsSpan()) < 0)
            {
                DecoderError("Data header verification failed.", data);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 根据数据创建完整帧数据
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns>完整帧数据</returns>
        public byte[] CreateFrame(ReadOnlySpan<byte> data)
        {
            byte[] frame = new byte[FrameExceptDataLength + data.Length];
            Headers.AsSpan().CopyTo(frame.AsSpan());
            data.CopyTo(frame.AsSpan().Slice(_headerLength));
            return frame;
        }

        /// <summary>
        /// 解析完整帧数据
        /// </summary>
        /// <param name="frame">完整帧数据</param>
        /// <returns>解析是否成功</returns>
        public override ByteFrameEventArgs DecodeFrame(ReadOnlySpan<byte> frame)
        {
            int idx = frame.IndexOf(Headers);
            if (idx == 0 && frame.Length >= Headers.Length)
            {
                return new(this, ByteFrameParams.Empty, Headers.Length, frame.Length - FrameExceptDataLength, frame.ToArray());
            }

            return new ByteFrameEventArgs(null, ByteFrameParams.Empty, frame.ToArray());
        }
    }
}
