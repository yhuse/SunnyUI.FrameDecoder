using Sunny.Com;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ComPort? com = null;
        private void button1_Click(object sender, EventArgs e)
        {
            com = new ComPort(Sunny.FrameDecoder.FrameDataType.Byte, "Com3");
            com.OnReceivedByte += Com_OnReceivedByte;
            com.OnReceivedString += Com_OnReceivedString;
            com.Open();
            Text = "Open";
        }

        private void Com_OnReceivedString(object? sender, Sunny.FrameDecoder.StringFrameEventArgs e)
        {
            Console.WriteLine(e.Value);
        }

        private void Com_OnReceivedByte(object sender, Sunny.FrameDecoder.ByteFrameEventArgs e)
        {
            Console.WriteLine(e.Value.ToArray().Length);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Enabled = false;
            Text = "";
            com?.Close();
            com?.Dispose();
            Text = "OK";
            button2.Enabled = true;
        }
    }
}
