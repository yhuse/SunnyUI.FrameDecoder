﻿using System;
using System.Threading;

namespace Sunny.FrameDecoder.Demo
{
    internal class TestStringFrameDecoder
    {
        public static void TestNMEA0183FrameDecoder()
        {
            var decoder = new NMEA0183FrameDecoder();
            //decoder.CacheTimeout = 0;
            decoder.OnDecoder += NMEA0183Decoder_OnDecoder;
            decoder.Decode("4,19.7,M,,,,0000*1F$GPGGA,092204.999,4250.5589,S,147");
            decoder.Decode("18.5084,E,1,04,24.4,19.7,M,,,,0000*1F$GPGSA,A,3,01,20,");
            decoder.Decode("19,13,,,,,,,,,40.4,24.4,32.2*0A$GPGSV,3,1,10,20,78,331,45,01,59,235,47,22,41,");
            decoder.Decode("069,,13,32,252,45*70$GPVTG,092204.999,42");
            Thread.Sleep(1000);
            decoder.Decode("4,19.7,M,,,,0000*1F$GPRMC,092204.999,4250.5589,S,147");
            decoder.Decode("18.5084,E,1,04,24.4,19.7,M,,,,0000*1F$GPGGA,A,3,01,20,");
        }

        public static void NMEA0183Decoder_OnDecoder(object sender, StringEventArgs e)
        {
            Console.WriteLine(e.Value);
        }

        public static void TestLineBasedFrameDecoder()
        {
            var decoder = new LineBasedFrameDecoder();
            //decoder.CacheTimeout = 0;
            decoder.OnDecoder += NMEA0183Decoder_OnDecoder;
            decoder.Decode("*AA" + '\r' + '\n' + "$GPGGA");
            decoder.Decode("*1F" + '\n' + '\r' + "$GPGSA");
            decoder.Decode("*0A" + '\r' + "$GPGSV");
            decoder.Decode("*70" + '\n' + "$GPVTG");
            Thread.Sleep(1000);
            decoder.Decode("*1F" + '\r' + "$GPRMC");
            decoder.Decode("*1F" + '\n' + "$GPGGA");
        }
    }
}
